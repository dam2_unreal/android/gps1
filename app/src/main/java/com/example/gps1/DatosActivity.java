package com.example.gps1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DatosActivity extends AppCompatActivity implements LocationListener{

    private EditText etNombre, etTelefono;
    private SeekBar sbEdad;
    private TextView tvLat2, tvLon2, tvEdad2;
    private TableLayout miTableLayout;
    private int edad;

    public final static int CODIGO_RESPUESTA=100;
    private int hasAccessFineLocation, hasAccessCoarseLocation;
    private double lat, lon;
    private LocationManager location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);

        cargarViews();
        cargarDatosMain();
        setListener();
    }

    //----------------------------------------------------------------------------------------------

    public void volver(View v){
        Intent i = new Intent();

        if(!etNombre.getText().toString().isEmpty()){
            i.putExtra(MainActivity.NOMBRE, etNombre.getText().toString().trim());
        }

        if(!etTelefono.getText().toString().isEmpty()){
            i.putExtra(MainActivity.TEL, etTelefono.getText().toString().trim());
        }

        if(!tvLat2.getText().toString().isEmpty()){
            i.putExtra(MainActivity.LAT, tvLat2.getText().toString());
            i.putExtra(MainActivity.LON, tvLon2.getText().toString());
        }

        i.putExtra(MainActivity.EDAD, edad);
        setResult(RESULT_OK, i);
        finish();
    }

    public void obtenerCoordenadas(View v){
        miTableLayout.setVisibility(View.VISIBLE);

        location = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if(location==null)
        {
            Toast.makeText(this, "Error al inicializar el GPS", Toast.LENGTH_SHORT).show();
            return;
        }

        hasAccessFineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        hasAccessCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        if(hasAccessCoarseLocation == PackageManager.PERMISSION_GRANTED && hasAccessFineLocation == PackageManager.PERMISSION_GRANTED)
        {
            location.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 20, (LocationListener) this);
        }
        else
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
            {
                AlertDialog.Builder informacion = new AlertDialog.Builder(this);
                informacion.setMessage("Los permisos son necesarios para bla bla bla...");
                informacion.setPositiveButton("Aceptar", null);
                informacion.show();
            }

            pedirPermisos();
        }
    }

    //----------------------------------------------------------------------------------------------

    private void pedirPermisos(){
        ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                }, CODIGO_RESPUESTA);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case CODIGO_RESPUESTA:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    location.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 20, (LocationListener) this);
                }else{
                    Toast.makeText(this, "Los permisos no han sido aceptados, vaya a la configuracion", Toast.LENGTH_SHORT).show();
                    return;
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //----------------------------------------------------------------------------------------------

    private void cargarViews() {
        etNombre = (EditText) findViewById(R.id.etNombre);
        etTelefono= (EditText) findViewById(R.id.etTelefono);
        tvEdad2 = (TextView) findViewById(R.id.tvEdad22);
        tvLat2 = (TextView) findViewById(R.id.tvVerLatitud);
        tvLon2 = (TextView) findViewById(R.id.tvVerLongitud);
        miTableLayout = (TableLayout) findViewById(R.id.miTableLayout);
        sbEdad = (SeekBar)  findViewById(R.id.sbEdad);
    }

    private void cargarDatosMain() {
        edad = 18;

        if(getIntent().hasExtra(MainActivity.NOMBRE)){
            etNombre.setText(getIntent().getStringExtra(MainActivity.NOMBRE));
        }

        if(getIntent().hasExtra(MainActivity.TEL)){
            etTelefono.setText(getIntent().getStringExtra(MainActivity.TEL));
        }

        if(getIntent().hasExtra(MainActivity.EDAD)){
            Log.d("\nDATOS EDAD>>>", getIntent().getStringExtra(MainActivity.EDAD)+"\n");
            edad = Integer.parseInt(getIntent().getStringExtra(MainActivity.EDAD));
        }

        sbEdad.setProgress(edad-18);
        tvEdad2.setText(String.format(getResources().getString(R.string.tvVerEdad), edad));
    }

    //----------------------------------------------------------------------------------------------

    private void setListener() {
        sbEdad.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                edad =18+progress;
                tvEdad2.setText(String.format(getResources().getString(R.string.tvVerEdad), edad));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();

        tvLat2.setText(""+lat);
        tvLon2.setText(""+lon);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
