package com.example.gps1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static final String NOMBRE = "n", EDAD="e", TEL="t", LAT="lat", LON="lon";
    public static final int REQUEST_CODE=1234;
    private TextView tvNombre, tvEdad, tvTelefono, tvLat1, tvLon1;
    private Button btnVerMaps;
    private double lat, lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cargarViews();
    }

//--------------------------------------------------------------------------------------------------

    private void cargarViews() {
        tvNombre = (TextView) findViewById(R.id.tvVerNombre);
        tvEdad = (TextView) findViewById(R.id.tvVerEdad);
        tvTelefono = (TextView) findViewById(R.id.tvVerTelefono);
        tvLat1 = (TextView) findViewById(R.id.tvVerLatitud);
        tvLon1 = (TextView) findViewById(R.id.tvVerLongitud);
        btnVerMaps = (Button) findViewById(R.id.btnMaps);
    }

    public void editar(View v){
        Intent i = new Intent(this, DatosActivity.class);
        Bundle datos = new Bundle();

        if(!tvNombre.getText().toString().isEmpty()){
            datos.putString(NOMBRE,tvNombre.getText().toString().trim());
        }

        if(!tvTelefono.getText().toString().isEmpty()){
            datos.putString(TEL,tvTelefono.getText().toString().trim());
        }

        if(!tvEdad.getText().toString().trim().isEmpty()){
            Log.d("ENTRANDO EN EDAD-->", tvEdad.getText().toString());
            datos.putString(EDAD,tvEdad.getText().toString().trim());
        }

        i.putExtras(datos);
        startActivityForResult(i, REQUEST_CODE);
    }

//--------------------------------------------------------------------------------------------------

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_CODE && resultCode==RESULT_OK){
            //All ha ido bien
            if(data.hasExtra(NOMBRE)){
                tvNombre.setText(data.getStringExtra(NOMBRE));
            }

            if(data.hasExtra(TEL)){
                tvTelefono.setText(data.getStringExtra(TEL));
            }

            if(data.hasExtra(LAT)){
                tvLat1.setText(data.getStringExtra(LAT));
                tvLon1.setText(data.getStringExtra(LON));
                btnVerMaps.setEnabled(true);

                lat = Double.parseDouble(data.getStringExtra(LAT));
                lon = Double.parseDouble(data.getStringExtra(LON));
            }else{
                btnVerMaps.setEnabled(false);
            }

            tvEdad.setText(""+data.getIntExtra(EDAD, 18));
        }else if(requestCode==REQUEST_CODE && resultCode==RESULT_CANCELED){
            //Hemos vuelto atras pero sin darle al boton
            Toast.makeText(this,"El usuario cancelo la operacion", Toast.LENGTH_SHORT).show();
        }
    }
    //Codigo de botn para ver mi posicion
    public void verMaps(View v){
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f",lat,lon);
        Intent i =  new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(i);
    }
}